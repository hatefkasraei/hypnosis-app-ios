//
//  BNRHypnosisView.m
//  Hypnosister
//
//  Created by Hatef Kasraei on 11/29/14.
//  Copyright (c) 2014 Hatef Kasraei. All rights reserved.
//

#import "BNRHypnosisView.h"

@interface BNRHypnosisView()
@property (strong, nonatomic) UIColor *circleColor;
@end

@implementation BNRHypnosisView
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    CGRect bounds = self.bounds;
    //figure out the center of the bounds rectangle:
    CGPoint center;
    center.x = bounds.origin.x + bounds.size.width / 2.0;
    center.y = bounds.origin.y + bounds.size.height / 2.0;
   
    //the largest circle will circumscribe the view
    float maxRadius = hypot(bounds.size.width, bounds.size.height) / 2.0;
    
    //define a path:
    //Instances of UIBezierPath class define and draw lines and curves that you can use to make shapes, like circles
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    for (float currentRadius = maxRadius; currentRadius > 0; currentRadius -= 20) {
        
        [path moveToPoint:CGPointMake(center.x + currentRadius, center.y)];
        [path addArcWithCenter:center radius:currentRadius startAngle:0.0 endAngle:M_PI * 2.0 clockwise:YES];
    }
    
    // configure the width of the line
    path.lineWidth = 10;
    
    //configure drawing color:
    [self.circleColor setStroke];
    
    //draw:
    [path stroke];
}

-(instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        //All BNRHypnosisViews start with a clear background
        
        self.backgroundColor = [UIColor clearColor];
        self.circleColor = [UIColor grayColor];
    }
    
    return self;
}

//when a fingure touches the screen
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@" %@ was touched", self);
    
    //get 3 random numbers between 1 and 3:
    float red = (arc4random() % 100) / 100.0;
    float blue = (arc4random() % 100) / 100.0;
    float green = (arc4random() % 100) / 100.0;
    
    UIColor *randomColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    self.circleColor = randomColor;
    
}

//setter of the new color to update view everytime the color has changed:
-(void) setCircleColor:(UIColor *)circleColor{
    _circleColor = circleColor;
    [self setNeedsDisplay];
}

@end
