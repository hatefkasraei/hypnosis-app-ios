//
//  main.m
//  Hypnosister
//
//  Created by Hatef Kasraei on 11/18/14.
//  Copyright (c) 2014 Hatef Kasraei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
